import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const button = document.querySelector(".button");
  button.addEventListener("click", () => {
    alert("💣");
  });

  let articles = [];
  let count = 5;
  const body = document.body;
  
  body.addEventListener("click", () => {
    if (articles.length == count) {
      return
    }
    articles = Array(count).fill(undefined).map((_, index) => createArticle(body, index + 1));
  });
});

function createArticle(body, index) {
  let article = document.createElement("article");
  article.className = "message";
  article.textContent = `Sample article ${index} content`;
  body.append(article);
  return article;
}